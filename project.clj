(defproject ssoup-rdf "0.1.0-SNAPSHOT"
  :description "Ssoup RDF library"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.293"]
                 [org.clojure/core.async "0.2.385"]
                 [org.clojure/core.cache "0.6.4"]
                 [org.clojure/core.memoize "0.5.6"]
                 [com.taoensso/timbre "4.0.2"]
                 [clj-time "0.9.0"]]
  :main ^:skip-aot ssoup-rdf.core
  :target-path "target/%s"
  ;:repositories {"my.datomic.com" {:url   "https://my.datomic.com/repo"
  ;                   :creds :gpg}}
  :profiles {:test {:dependencies [;[com.datomic/datomic-pro "0.9.5206"]
                                   [org.apache.jena/apache-jena-libs "3.0.1" :extension "pom"]]}
             :uberjar {:aot :all}}
  :repositories [["releases" {:url "https://clojars.org/repo/"
	  						  :username :env/CLOJARS_USERNAME
  							  :password :env/CLOJARS_PASSWORD}]])

