(ns ssoup-rdf.core
  (:require #?(:clj [clojure.data.json :as json])))

(defn dispatch-on-ts-type [ts & _]
  (:type ts))

; defprotocol ITripleStore
(defmulti close dispatch-on-ts-type)
(defmulti union-triple-store
          "Return the union of 2 triple stores.
          Changes to either ts1 or ts2 will be reflected in the result triple store, and vice versa: specifically, additions
          to and removals from the union will be implemented as operations on ts1 only." dispatch-on-ts-type)
(defmulti save-data "Save data into the triple store" dispatch-on-ts-type)
(defmulti delete-data "Delete data from the triple store" dispatch-on-ts-type)
(defmulti delete-data-by-id "Delete all data registered under the given id from the triple store" dispatch-on-ts-type)
(defmulti read-file "Read data from a file and save it into the triple store"
          dispatch-on-ts-type)
(defmulti add-triple dispatch-on-ts-type)
(defmulti get-triple dispatch-on-ts-type)
(defmulti list-subjects-with-property dispatch-on-ts-type)
(defmulti get-triple-object dispatch-on-ts-type)
(defmulti get-triples dispatch-on-ts-type)
(defmulti get-object dispatch-on-ts-type)
(defmulti blank-node dispatch-on-ts-type)
(defmulti push dispatch-on-ts-type)
(defmulti pull dispatch-on-ts-type)
(defmulti pull-many dispatch-on-ts-type)
(defmulti print-all dispatch-on-ts-type)
(defmulti exec-query dispatch-on-ts-type)
(defmulti exec-query-single-result dispatch-on-ts-type)
(defmulti get-data-by-type "Returns all the objects of a given type" dispatch-on-ts-type)
(defmulti get-model-rdf-context "Returns the model rdf context" dispatch-on-ts-type)
(defmulti union-ts "Returns a union triple store from the set of provided triple stores" dispatch-on-ts-type)

; defprotocol IOntology
(defmulti get-schema #_[tx ctx]
          "Returns the schema info in the triple store. A map from type id to type definition is returned."
          dispatch-on-ts-type)

(defmulti list-types #_[tx ctx]
          "Returns the list of types defined in the triple store."
          dispatch-on-ts-type)

(defmulti get-declared-properties #_[ts ctx cl direct]
          "Returns all the declared properties of a given RDFS class. If direct is true
           only the properties of the class specified are returned and not those of the
           superclasses."
          dispatch-on-ts-type)

(defmulti get-super-types #_[ts ctx type]
          "Returns all the super types of a given RDFS class."
          dispatch-on-ts-type)

(defmethod get-triple-object :default
  [ts rdf-ctx s p]
  (let [triple (get-triple ts rdf-ctx s p)]
    (nth triple 2)))

(defn boolean? [x]
  (instance? Boolean x))

(defn literal? [v]
  (or (string? v) (integer? v) (boolean? v)))

(defn iri? [v]
  (and (string? v) (or (.startsWith v "http://") (.startsWith v "https://"))))

(defn anon? [n]
  (and (keyword? n) (= (namespace n) "_")))

; to have faster parsing we could use Transit but would mean to add a dependency:
; in the future give the user the possibility to provaide an external parser.
(defn- parse-json [json-data]
  #?(:clj (json/read-str json-data))
  #?(:cljs (js->clj (.parse js/JSON json-data))))

(defmethod union-ts :default [ctx tss & name]
  (let [ts-type (distinct (map :type tss))]
    (if (> (count ts-type) 1)
      (throw (ex-info "Cannot make the union of triple stores of different type" {}))
      (apply union-ts {:type (first ts-type)} ctx tss name))))

;-------------------------
; Hierarchy relations needed for correct treatment of rdf data.
;-------------------------

(derive :rdf/List :rdfs/Resource)

;-------------------------
; CLJ -> Serialized RDF
;-------------------------
(defn to-rdf-expanded-iri [ts-or-rdf-ctx k]
  "Convert a term in form :<abbreviated namespace>/<term> into a string of the form
   <expanded namespace> + <term>"
  (let [rdf-ctx (if (isa? (:type ts-or-rdf-ctx) :ssoup/triple-store)
                  (get-model-rdf-context ts-or-rdf-ctx)
                  ts-or-rdf-ctx)]
    (when (and k (keyword? k))
      (let [ns (namespace k)
            n (name k)
            xns (get rdf-ctx (keyword ns))]
        (if (and ns (nil? xns))
          (do
            (println "WARN Cannot expand " k ". No mapping for " ns " in context " rdf-ctx)
            ; (throw (ex-info (str "Cannot expand " k ". No mapping for " ns) {}))
            nil)
          (str xns n))))))

(defn to-rdf-compact-iri [k]
  "Convert a term in form :<abbreviated namespace>/<term> into a string of the form
   <abbreviated namespace>:<term>"
  (when (and k (keyword? k))
    (let [ns (namespace k)
          n (name k)]
      (if ns
        (str ns ":" n)
        n))))

(defn to-rdf-jsonld-vec [data])

(defn predicate-object-to-jsonld [p o]
  [(cond
     (= p :id) ["\"@id\""]
     (= p :type) ["\"@type\""]
     (= p :default) ["\"@vocab\""]
     (= p :value) ["\"@value\""]
     (= p :lang) ["\"@language\""]
     (keyword? p) ["\"" (to-rdf-compact-iri p) "\""]
     (string? p) ["\"" p "\""])
   ":"
   (if (keyword? o)
     (if (or (= p :id) (= p :type))
       ["\"" (to-rdf-compact-iri o) "\""]
       ["{\"@id\":\"" (to-rdf-compact-iri o) "\"}"])
     (to-rdf-jsonld-vec o))])

(defn to-rdf-jsonld-vec [data]
  (cond
    (string? data) [(str "\"" data "\"")]
    (keyword? data) ["{\"@id\":\"" (to-rdf-compact-iri data) "\"}"]
    (map? data) (flatten ["{"
                          (interpose ","
                                     (->> data
                                          (filter (fn [[p o]]
                                                    (and (not (= p :rdf/context))
                                                         (not (nil? o))))) ; omit properties with null object
                                          (map (fn [[p o]]
                                                 (predicate-object-to-jsonld p o)))))
                          "}"])
    (set? data) (flatten ["["
                          (interpose ","
                                     (map (fn [el]
                                            (to-rdf-jsonld-vec el)) data))
                          "]"])
    (or (seq? data)
        (vector? data)) (flatten ["{\"@list\": ["
                          (interpose ","
                                     (map (fn [el]
                                            (to-rdf-jsonld-vec el)) data))
                          "]}"])
    :else [data]))

(defn to-rdf-jsonld
  ([data]
   (to-rdf-jsonld nil data))
  ([rdf-ctx data]
   (let [rdf-ctx (or rdf-ctx (:rdf/context data))]
     (apply str (flatten
                  ["{" "\"@context\":"
                   (to-rdf-jsonld-vec rdf-ctx)
                   (if (:graph data)
                     [",\"@graph\":" (to-rdf-jsonld-vec (:graph data))]
                     [",\"@graph\":[" (to-rdf-jsonld-vec data) "]"])
                   "}"])))))

(defn to-rdf [rdf-ctx data data-format]
  (condp = data-format
    "JSON-LD" (to-rdf-jsonld rdf-ctx data)))

;-------------------------
; CLJ utilities
;-------------------------

(defn context-prefix [context iri]
  "Returns the prefix identifying the given iri in the provided context.
   Returns nil if no match could be found in the context"
  (loop [context context]
    (when-not (empty? context)
      (let [[prefix ctx-iri] (first context)]
        (if (= ctx-iri iri)
          prefix
          (recur (rest context)))))))

(defn expanded-iri-to-clj-keyword
  "Converts an expanded iri to a clojure keyword in the given context"
  [expanded-iri rdf-ctx]
  (let [i (clojure.string/last-index-of expanded-iri "/")
        ctx-iri (subs expanded-iri 0 (inc i))
        prefix (context-prefix rdf-ctx ctx-iri)
        local-name (subs expanded-iri i)]
    (if prefix
      (keyword prefix local-name)
      expanded-iri)))

(defn compact-iri-to-clj-keyword
  "Converts a compact iri of the form <abbreviated namespace>:<term>
   to a clojure keyword in the given context"
  [compact-iri]
  (let [i (clojure.string/index-of compact-iri ":")]
    (if i
      (let [prefix (subs compact-iri 0 i)
            local-name (subs compact-iri (inc i))]
        (keyword prefix local-name))
      (keyword compact-iri))))

(defn expanded-iri? [iri]
  (clojure.string/includes? iri "//"))

(defn iri-to-clj-keyword [iri rdf-ctx]
  (if (expanded-iri? iri)
    (expanded-iri-to-clj-keyword iri rdf-ctx)
    (compact-iri-to-clj-keyword iri)))

(defn with-context
  "Converts the passed object to the new rdf context provided.
   If some uri used by the object is not mapped in the new context,
   an error is raised."
  ([object new-rdf-context]
   (with-context object (:rdf/context object) new-rdf-context))
  ([object old-rdf-context new-rdf-context]
   (cond
     (or (= object :id)
         (= object :type)
         (= object :graph)
         (= object :lang)
         (= object :value))
     object

     (keyword? object)
     (let [old-prefix (or (keyword (namespace object)) :default)
           uri (get old-rdf-context old-prefix)
           new-prefix (context-prefix new-rdf-context uri)]
       (if new-prefix
         (if (= new-prefix :default)
           (keyword (name object))
           (keyword (name new-prefix) (name object)))
         (throw (ex-info (str "No mapping for " old-prefix " <" uri "> in the context provided. Object: " object " New rdf/context " new-rdf-context) {}))))

     (map? object)
     (->> object
          (map (fn [[k v]]
                 (if (= :rdf/context k)
                   [k new-rdf-context]
                   [(with-context k old-rdf-context new-rdf-context)
                    (with-context v old-rdf-context new-rdf-context)])))
          (into {}))

     (set? object)
     (->> object
          (map (fn [el]
                 (with-context el old-rdf-context new-rdf-context)))
          (into #{}))

     :else
     object)))

(defn remove-uri-from-context
  "Removes a specific uri from a context map.
   All entries with the specific uri as value will be removed."
  [rdf-ctx uri]
  (->> rdf-ctx
       (filter (fn [[k v]]
                 (not (= v uri))))
       (into {})))

(defn ensure-entries-in-context
  "Ensures that the given map of entries is included in the context.
   If some of the uris are already in the context but with a different abbreviation,
   the abbreviation will be changed to the one specified."
  [rdf-ctx entries-to-ensure]
  (let [new-rdf-ctx (reduce (fn [rdf-ctx [k v]]
                              (remove-uri-from-context rdf-ctx v))
                            rdf-ctx entries-to-ensure)]
    (let [used-prefixes (clojure.set/intersection (set (keys new-rdf-ctx))
                                                  (set (keys entries-to-ensure)))]
      (when (not-empty used-prefixes)
        (throw (ex-info "Some prefixes are already used with different uris: " {:used-prefixes used-prefixes
                                                                                :orig          (select-keys new-rdf-ctx used-prefixes)
                                                                                :new           (select-keys entries-to-ensure used-prefixes)}))))
    (merge new-rdf-ctx entries-to-ensure)))

;-------------------------
; Serialized RDF -> CLJ
;-------------------------

(defn parse-jsonld-key [k rdf-ctx]
  (cond
    (= k "@id")
    :id

    (= k "@type")
    :type

    (= k "@vocab")
    :default

    (= k "@graph")
    :graph

    (= k "@language")
    :lang

    (= k "@value")
    :value

    :else
    (iri-to-clj-keyword k rdf-ctx)))

(defn parse-jsonld-data [jsonld rdf-ctx]
  (cond
    (map? jsonld)
    (cond
      ; in case the map contains a single entry @id convert it to a keyword
      (and (= (count jsonld) 1)
             (= (key (first jsonld)) "@id"))
      (let [id (iri-to-clj-keyword (val (first jsonld)) rdf-ctx)]
        (if (keyword? id)
          id
          {:id id})) ; if the id could not be converted to a keyword

      ; in case the map contains a single entry @list convert the result to an array
      (and (= (count jsonld) 1)
           (= (key (first jsonld)) "@list"))
      (let [list-elements (val (first jsonld))]
        (into [] (parse-jsonld-data list-elements rdf-ctx)))

      :else
      (->> jsonld
           (filter (fn [[k _]] (not (= k "@context"))))
           (map (fn [[k v]]
                  (if (or (= k "@id") (= k "@type"))
                    [(parse-jsonld-key k rdf-ctx)
                     (iri-to-clj-keyword v rdf-ctx)]
                    [(parse-jsonld-key k rdf-ctx)
                     (parse-jsonld-data v rdf-ctx)])))
           (into {})))

    (or (seq? jsonld) (vector? jsonld))
    (->> jsonld
         (map (fn [x]
                (parse-jsonld-data x rdf-ctx)))
         (into #{}))

    :else
    jsonld))

(defn parse-jsonld-context [context]
  (cond
    (string? context)
    {:default context}

    (map? context)
    (reduce (fn [res [k v]]
              (merge res
                     {(parse-jsonld-key k {})
                      (parse-jsonld-data v {})}))
            {} context)

    (or (seq? context) (vector? context))
    (reduce (fn [res ctx]
              (merge res (parse-jsonld-context ctx)))
            {} context)))

(defn from-rdf-jsonld
  "For now this will only work with compacted JSON-LD"
  [rdf-data]
  (let [jsonld (parse-json rdf-data)
        jsonld-context (get jsonld "@context")
        rdf-ctx (parse-jsonld-context jsonld-context)
        rdf-data (parse-jsonld-data jsonld rdf-ctx)]
    (merge {:rdf/context rdf-ctx}
           rdf-data)))

(defn from-rdf [rdf-data data-format]
  (condp = data-format
    "JSON-LD" (from-rdf-jsonld rdf-data)))

;-------------------------
; RDF Triples -> CLJ
;-------------------------
(defn to-clj-iri [rdf-ctx v]
  "Convert a term in form <expanded namespace> + <term> into
  a compact clojure abbreviated keyword: <abbreviated namespace>/<term>"
  (if (and v (iri? v))
    (let [[cns xns] ;(first (reverse (sort-by (fn [[cns _]] (count (name cns))) (filter (fn [[_ xns]] (.startsWith v xns)) rdf-ctx))))
          (->> rdf-ctx
               (filter (fn [[_ xns]] (.startsWith v xns)))
               (sort-by (fn [[_ xns]] (count xns)))
               (reverse)
               (first))
          n (subs v (count xns))]
      (if xns
        (do
          (when (and (not (.endsWith xns "/"))
                     (not (.endsWith xns "#"))
                     (not (= xns v)))
            ; when the prefix does not end in / or # it should identify a fully qualified resource
            (println "Warning: iri contains prefix not ending in / or #"))
          (keyword (name cns) n))
        cns)) ; namespace exact match,no suffix.
    v))

(defn keep-most-specific [ts types]
  "Given a collection of types a set is returned with only the most specific ones"
  (let [h-ref (:h-ref ts)
        isa? (if h-ref (partial isa? @h-ref) isa?)
        ms (reduce (fn [r-types t]
              (let [r-types (map (fn [rt]
                                   (if (isa? t rt)
                                     t
                                     rt)) r-types)]
                (if (some (fn [rt] (isa? rt t)) r-types)
                  (into #{} r-types)
                  (into #{} (conj r-types t)))))
            #{} types)]
    #_(println "Reducing " types " => " ms)
    ms))

(defn get-rdf-types [ts rdf-ctx s]
  (let [triples (get-triples ts rdf-ctx s :rdf/type)]
    (if (= (count triples) 0)
      nil
      (if (= (count triples) 1)
        (nth (first triples) 2)
        (let [types (into #{} (map #(nth % 2) triples))
              types (keep-most-specific ts types)] ; only keep the most specific types
          (if (= (count types) 1)
            (first types)
            types))))))

(defn has-rdf-type? [ts rdf-ctx s type]
  (let [types (get-rdf-types ts rdf-ctx s)
        types (if (set? types) types #{types})]
    (some #{type} types)))

(defn rdf-list? [ts rdf-ctx s]
  (or
    (= :rdf/nil s)
    (has-rdf-type? ts rdf-ctx s :rdf/List)))

(defn to-triples [gen-blank-node s data]
  (if (and data (map? data))
    (let [s (or s (:id data))
          data (dissoc data :id)]
      (->>
        (map (fn [[k v]]
               (cond
                 (or (string? v) (number? v) (keyword? v)) [[s k v]]
                 (map? v) (let [n (:id v)
                                n (if n n (gen-blank-node))]
                            (apply conj [[s k n]] (to-triples gen-blank-node n v)))
                 (set? v) (reduce concat (map (fn [vi] (to-triples gen-blank-node s {k vi})) v))
                 (or (seq? v) (vector? v)) (if (empty? v)
                                             [[s k :rdf/nil]]
                                             (let [bn (gen-blank-node)]
                                               (apply conj
                                                      (apply conj
                                                             [[s k bn]
                                                              [bn :rdf/type :rdf/List]]
                                                             (to-triples gen-blank-node bn {:rdf/first (first v)}))
                                                      (to-triples gen-blank-node bn {:rdf/rest (rest v)}))))))
             data)
        (reduce concat)
        (into [])))
    []))

(defmethod push :default [ts rdf-ctx data]
  (let [s (:id data)
        triples (to-triples #(blank-node ts) s data)]
    (doall (map (fn [t] (add-triple ts rdf-ctx t)) triples))))

(defn- pull-pattern [ts rdf-ctx pull-cache s pattern options])

(defmethod pull :default
  ([ts rdf-ctx s pattern]
    "Pull syntax pattern grammar: (See http://docs.datomic.com/pull.html)

      pattern            = [attr-spec+]
      attr-spec          = attr-name | wildcard | map-spec | attr-expr
      attr-name          = an edn keyword that names an attr
      wildcard           = \"*\" or '*'
      map-spec           = { ((attr-name | limit-expr) (pattern | recursion-limit))+ }
      attr-expr          = limit-expr | default-expr
      limit-expr         = [(\"limit\" | 'limit') attr-name (positive-number | nil)]
      default-expr       = [(\"default\" | 'default') attr-name any-value]
      recursion-limit    = positive-number | '...'
    "
    (pull ts rdf-ctx s pattern {}))
  ([ts rdf-ctx s pattern options]
    (pull-pattern ts rdf-ctx (atom {}) s pattern options)))

(defn- pattern? [str]
  (vector? str))

(defn- wildcard? [attr-spec]
  (or (= attr-spec "*") (= attr-spec '*')))

(defn- default-expr? [attr-spec]
  (and (list? attr-spec) (= (first attr-spec) (symbol "default"))))

(defn- limit-expr? [attr-spec]
  (and (list? attr-spec) (= (first attr-spec) (symbol "limit"))))

(defn- pull-default-expr [ts rdf-ctx pull-cache s attr-spec options]
  (let [attr-name (second attr-spec)
        default-value (nth attr-spec 3)]))

(defn- pull-limit-expr [ts rdf-ctx pull-cache s attr-spec options]
  (let [attr-name (second attr-spec)
        limit (nth attr-spec 3)]))

(defn- pull-attr [ts rdf-ctx pull-cache s attr-name pattern all-levels options])

(defn- pull-list [ts rdf-ctx pull-cache s pattern all-levels options])

(defn- pull-all [ts rdf-ctx pull-cache s all-levels options])

(defn- pull-object [ts rdf-ctx pull-cache o pattern all-levels options]
  (cond
    (literal? o) o
    pattern (pull-pattern ts rdf-ctx pull-cache o pattern options)
    (> all-levels 0) (if (rdf-list? ts rdf-ctx o)
                       (pull-list ts rdf-ctx pull-cache o pattern all-levels options)
                       (pull-all ts rdf-ctx pull-cache o all-levels options))
    :else o))

(defn- pull-list [ts rdf-ctx pull-cache s pattern all-levels options]
  (if (<= all-levels 0)
    s
    (if (= :rdf/nil s)
      []
      (let [o (get-triple-object ts rdf-ctx s :rdf/first)
            rest-node (get-triple-object ts rdf-ctx s :rdf/rest)]
        (into []
              (cons (pull-object ts rdf-ctx pull-cache o pattern (dec all-levels) options)
                    (pull-list ts rdf-ctx pull-cache rest-node pattern all-levels options)))))))

(defn- pull-attr [ts rdf-ctx pull-cache s attr-name pattern all-levels options]
  (if (= :rdf/type attr-name)
    (let [types (get-rdf-types ts rdf-ctx s)]
      ;(println "s " s "types " types)
      {attr-name types})
    (let [triples (get-triples ts rdf-ctx s attr-name)
          attrs-options (:attrs options)
          attr-options (when attrs-options (get attrs-options attr-name))
          pull-attr-object (fn [o]
                             (if (or
                                   (nil? o)
                                   (literal? o)
                                   (some #(= :ssoup/no-expand %) attr-options))
                               o
                               (pull-object ts rdf-ctx pull-cache o pattern all-levels options)))]
      (if (empty? triples)
        nil
        {attr-name (if (or (> (count triples) 1) (some #(= :ssoup/as-set %) attr-options))
                     (into #{} (map #(pull-attr-object (nth % 2)) triples))
                     (let [o (nth (first triples) 2)] (pull-attr-object o)))}))))

(defn- pull-attr-many-with-pattern [ts rdf-ctx pull-cache limit-expr pattern options])

(defn- pull-attr-recursive [ts rdf-ctx pull-cache attr-name recursive-limit options])

(defn- pull-attr-many-recursive [ts rdf-ctx pull-cache limit-expr recursive-limit options])

(defn- pull-map [ts rdf-ctx pull-cache s map-spec options]
  (apply merge
         (map (fn [[a v]]
                (if (limit-expr? a)
                  (if (pattern? v)
                    (pull-attr-many-with-pattern ts rdf-ctx pull-cache a v options)
                    (pull-attr-many-recursive ts rdf-ctx pull-cache a v options))
                  (if (pattern? v)
                    (pull-attr ts rdf-ctx pull-cache s a v 0 options)
                    (pull-attr-recursive ts rdf-ctx pull-cache a v options))))
              map-spec)))

(defn- list-attrs [ts rdf-ctx s]
  (let [triples (get-triples ts rdf-ctx s)]
    (distinct (map (fn [[s p o]] p) triples))))

(defn- pull-all [ts rdf-ctx pull-cache s all-levels options]
  (if (<= all-levels 0)
    s
    (if (contains? @pull-cache s)
      (get @pull-cache s)
      (do
        (swap! pull-cache assoc s s) ; temp value , needed in case of loops in the data
        (let [attrs (list-attrs ts rdf-ctx s)
              r (apply merge
                       (map (fn [p]
                              (pull-attr ts rdf-ctx pull-cache s p nil (dec all-levels) options)) attrs))]
          (if (nil? r)
            s
            (do
              (swap! pull-cache assoc s r)
              (if (anon? s)
                r
                (assoc r :id s)))))))))

(defn- pull-attr-with-spec [ts rdf-ctx pull-cache s attr-spec options]
  (cond
    (map? attr-spec) (pull-map ts rdf-ctx pull-cache s attr-spec options)
    (default-expr? attr-spec) (pull-default-expr ts rdf-ctx pull-cache s attr-spec options)
    (limit-expr? attr-spec) (pull-limit-expr ts rdf-ctx pull-cache s attr-spec options)
    :else (pull-attr ts rdf-ctx pull-cache s attr-spec nil 0 options)))

(defn- pull-pattern [ts rdf-ctx pull-cache s pattern options]
  (let [r (if (wildcard? (first pattern))
            (let [all-levels (second pattern)
                  all-levels (or all-levels 1)]
              (pull-object ts rdf-ctx pull-cache s nil all-levels options))
            (apply merge
                   (map (fn [attr-spec]
                          (pull-attr-with-spec ts rdf-ctx pull-cache s attr-spec options))
                        pattern)))]
    (if (nil? r)
      s
      (if (or (anon? s) (not (map? r)))
        r
        (assoc r :id s)))))

(defn build-query-string-atom [a]
  (cond
    (literal? a) a
    (keyword? a) (if (.startsWith (name a) "?")
                   (name a)
                   (to-rdf-compact-iri a))))

(defn build-query-string-where-clause [q]
  (let [where (:where (:query q))]
    (str "where { "
         (apply str
                (map (fn [[s p o]]
                       (str (build-query-string-atom s) " "
                            (build-query-string-atom p) " "
                            (build-query-string-atom o) " "
                            ". "))
                     where))
         "} ")))

(defn build-query-string [q]
  (let [query (:query q)
        select (:select query)
        order-by (:order-by query)
        limit (:limit q)
        offset (:offset q)]
    (.trim (str "select " (build-query-string-atom select) " "
                (build-query-string-where-clause q)
                (when order-by (apply str "order by " (map (fn [[k m]] (str (name m) "(" (name k) ") ")) order-by)))
                (when limit (str "limit " limit " "))
                (when offset (str "offset " offset " "))))))

(defn build-count-query-string [q]
  (let [query (:query q)
        select (:select query)
        a (build-query-string-atom select)]
    (.trim (str "select (count(*) as ?count) "
                (build-query-string-where-clause q)))))

(defmethod pull-many :default
  ([ts rdf-ctx q pattern]
    (pull-many ts rdf-ctx q pattern {}))
  ([ts rdf-ctx q pattern options]
    (let [var (:select (:query q))
          var (keyword (.replace (name var) "?" ""))
          query-string (build-query-string q)
          count-query-string (build-count-query-string q)
          offset (or (:offset q) 0)
          results (exec-query ts rdf-ctx query-string)
          total-count (:count (exec-query-single-result ts rdf-ctx count-query-string))]
      {:ssoup/from       offset
       :ssoup/to         (dec (+ offset (count results)))
       :ssoup/totalCount total-count
       :ssoup/results    (doall (for [res results]
                                  (pull ts rdf-ctx (get res var) pattern options)))})))

(def schema-org-context {:cat         "http://www.w3.org/ns/dcat#"
                         :qb          "http://purl.org/linked-data/cube#"
                         :org         "http://www.w3.org/ns/org#"
                         :grddl       "http://www.w3.org/2003/g/data-view#"
                         :ma          "http://www.w3.org/ns/ma-ont#"
                         :owl         "http://www.w3.org/2002/07/owl#"
                         :rdf         "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                         :rdfa        "http://www.w3.org/ns/rdfa#"
                         :rdfs        "http://www.w3.org/2000/01/rdf-schema#"
                         :rif         "http://www.w3.org/2007/rif#"
                         :rr          "http://www.w3.org/ns/r2rml#"
                         :skos        "http://www.w3.org/2004/02/skos/core#"
                         :skosxl      "http://www.w3.org/2008/05/skos-xl#"
                         :wdr         "http://www.w3.org/2007/05/powder#"
                         :void        "http://rdfs.org/ns/void#"
                         :wdrs        "http://www.w3.org/2007/05/powder-s#"
                         :xhv         "http://www.w3.org/1999/xhtml/vocab#"
                         :xml         "http://www.w3.org/XML/1998/namespace"
                         :xsd         "http://www.w3.org/2001/XMLSchema#"
                         :prov        "http://www.w3.org/ns/prov#"
                         :sd          "http://www.w3.org/ns/sparql-service-description#"
                         :gldp        "http://www.w3.org/ns/people#"
                         :cnt         "http://www.w3.org/2008/content#"
                         :dcat        "http://www.w3.org/ns/dcat#"
                         :earl        "http://www.w3.org/ns/earl#"
                         :ht          "http://www.w3.org/2006/http#"
                         :ptr         "http://www.w3.org/2009/pointers#"
                         :cc          "http://creativecommons.org/ns#"
                         :ctag        "http://commontag.org/ns#"
                         ;:dc          "http://purl.org/dc/terms/"
                         ; to be compatible with owl (https://www.w3.org/2006/vcard/ns#)
                         :dc          "http://purl.org/dc/elements/1.1/"
                         :cterms      "http://purl.org/dc/terms/"
                         :foaf        "http://xmlns.com/foaf/0.1/"
                         :gr          "http://purl.org/goodrelations/v1#"
                         :ical        "http://www.w3.org/2002/12/cal/icaltzd#"
                         :og          "http://ogp.me/ns#"
                         :rev         "http://purl.org/stuff/rev#"
                         :sioc        "http://rdfs.org/sioc/ns#"
                         :v           "http://rdf.data-vocabulary.org/#"
                         :vcard       "http://www.w3.org/2006/vcard/ns#"
                         :schema      "http://schema.org/"
                         :describedby "http://www.w3.org/2007/05/powder-s#describedby"
                         :license     "http://www.w3.org/1999/xhtml/vocab#license"
                         :role        "http://www.w3.org/1999/xhtml/vocab#role"})
