(ns ssoup-rdf.jena-ts
  (require
    [clojure.set]
    [ssoup-rdf.core :as ssoup-rdf])
  (:import (java.util Date)
           (org.apache.jena.rdf.model ModelFactory Model Resource Property RDFNode AnonId Statement ResIterator)
           (org.apache.jena.query QueryExecutionFactory ResultSetFormatter ParameterizedSparqlString ResultSet DatasetFactory Dataset)
           (org.apache.jena.ontology OntModelSpec OntClass OntProperty OntResource OntModel)
           (java.io StringReader)
           (org.apache.jena.sparql.core DatasetGraphFactory)
           (org.apache.jena.riot RDFDataMgr RDFFormat)
           (org.apache.jena.vocabulary OWL)))

(derive :ssoup-ts/jena :ssoup/triple-store)

(defn new-plain-jena-triple-store
  ([ts-name tags]
   (let [model (ModelFactory/createOntologyModel OntModelSpec/OWL_DL_MEM_RDFS_INF)]
     (new-plain-jena-triple-store ts-name tags model)))
  ([ts-name tags model]
   {:type      :ssoup-ts/jena
    :name      ts-name
    :data-tags tags
    :model     model}))

(defn new-jena-triple-store
  ([ts-name tags h-ref]
   (new-jena-triple-store ts-name tags h-ref {}))
  ([ts-name tags h-ref {inference-type :inference-type}]
   (let [dataset (DatasetFactory/createTxnMem)
         ; by default query the default model
         model (.getNamedModel dataset "urn:x-arq:UnionGraph")
         model (ModelFactory/createOntologyModel
                 (condp = inference-type
                   :inf-rdfs OntModelSpec/OWL_DL_MEM_RDFS_INF
                   OntModelSpec/OWL_MEM)
                 model)]
     {:type      :ssoup-ts/jena
      :name      ts-name
      :data-tags tags
      :h-ref     h-ref
      :dataset   dataset
      :model     model}))
  ([ts-name tags h-ref uri model]                           ; create a new Dataset and immediately add a Model
   (let [ts (new-jena-triple-store ts-name tags h-ref)
         ^Dataset dataset (:dataset ts)]
     (.addNamedModel dataset uri model)
     ts)))

(defn test-ont []
  (let [ont-spec OntModelSpec/OWL_DL_MEM_RDFS_INF
        ^OntModel m1 (ModelFactory/createOntologyModel ont-spec)
        t1 (.createClass m1 "http://www.test.com/t1")
        ^OntModel m2 (ModelFactory/createOntologyModel ont-spec)
        r1 (.createIndividual m2 "test:r1" t1)
        ;_ (RDFDataMgr/write System/out m1 RDFFormat/NTRIPLES)
        dataset (DatasetFactory/createTxnMem)
        model (.getNamedModel dataset "urn:x-arq:UnionGraph")
        ^OntModel mu (ModelFactory/createOntologyModel ont-spec model)
        _ (.addNamedModel dataset "m1" m1)
        _ (.addNamedModel dataset "m2" m2)
        t11 (.getOntResource mu "http://www.test.com/t1")
        cl (.asClass t11)
        _ (println cl)
        _ (RDFDataMgr/write System/out model RDFFormat/NTRIPLES)]))

; -------------------------------------------------------
; ITripleStore
; -------------------------------------------------------

(defmethod ssoup-rdf/union-triple-store :ssoup-ts/jena [ts1 ts2 {inference-type :inference-type}]
  (when (not= (:type ts2) :ssoup-ts/jena)
    (throw (ex-info "Cannot union with a non Jena triple store" {:ts1 ts1
                                                                 :ts2 ts2})))
  (let [m1 (:model ts1)
        m2 (:model ts2)
        mu (ModelFactory/createUnion m1 m2)
        ont-spec (condp = inference-type
                   :inf-rdfs OntModelSpec/OWL_DL_MEM_RDFS_INF
                   OntModelSpec/OWL_MEM)
        omu (ModelFactory/createOntologyModel ont-spec mu)]
    (assoc ts1 :model mu)))

(defmethod ssoup-rdf/get-model-rdf-context :ssoup-ts/jena [ts]
  (clojure.walk/keywordize-keys (into {} (.getNsPrefixMap (:model ts)))))

(defmethod ssoup-rdf/union-ts :ssoup-ts/jena [_ ctx tss & name]
  (let [union-model (reduce (fn [ma mb] (.union ma mb)) (map :model tss))
        tags-intersection (reduce clojure.set/intersection (map :data-tags tss))]
    {:type      :ssoup-ts/jena
     :name      name
     :data-tags tags-intersection
     :model     union-model}))

(defmethod ssoup-rdf/close :ssoup-ts/jena [ts]
  (.close (:model ts)))

(defmethod ssoup-rdf/save-data :ssoup-ts/jena [ts uri data type]
  (with-open [data-reader (StringReader. data)]
    (let [^Dataset dataset (:dataset ts)
          ^Model union-model (:model ts)
          ^Model model (ModelFactory/createOntologyModel OntModelSpec/OWL_DL_MEM_RDFS_INF)]
      (.read model data-reader nil ^String type)
      ; update the prefixes in the union model too
      (.setNsPrefixes union-model (.getNsPrefixMap model))
      (.addNamedModel dataset uri model))))

(defmethod ssoup-rdf/delete-data :ssoup-ts/jena [ts data format]
  (with-open [data-reader (StringReader. data)]
    (let [model-with-data-to-delete (ModelFactory/createDefaultModel)]
      (.read model-with-data-to-delete data-reader nil ^String format)
      ;(.write (:model ts) System/out "N-Triples")
      ;(.write model-with-data-to-delete System/out "N-Triples")
      #_(.remove (:model ts) model-with-data-to-delete))))

(defmethod ssoup-rdf/delete-data-by-id :ssoup-ts/jena
  ([ts id]
    (let [rdf-ctx (ssoup-rdf/get-model-rdf-context ts)]
      (ssoup-rdf/delete-data-by-id ts rdf-ctx id)))
  ([ts rdf-ctx id]
    (let [dataset (:dataset ts)
          iri (ssoup-rdf/to-rdf-expanded-iri rdf-ctx id)]
      (when iri
        (.removeNamedModel dataset iri)))))

(defmethod ssoup-rdf/read-file :ssoup-ts/jena [ts filename format]
  (.read (:model ts) filename format))

(defmethod ssoup-rdf/print-all :ssoup-ts/jena [ts format]
  (.write (:model ts) System/out format))

(defn clj-to-jena
  ([^Model m ctx o]
   (clj-to-jena m ctx o true))
  ([^Model m ctx o throw-exception-if-ns-unknown]
   (when o
     (if (keyword? o)
       (if (= "_" (namespace o))
         (.createResource m (new AnonId (name o)))
         (if-let [expanded-iri (ssoup-rdf/to-rdf-expanded-iri ctx o)]
           (.createResource m expanded-iri)
           (when throw-exception-if-ns-unknown
             (throw (Exception. (str "Cannot expand keyword " o " in the given rdf context"))))))
       o))))

(defn clj-to-jena-ont [^OntModel m ctx o]
  (when o
    (if (keyword? o)
      (if (= "_" (namespace o))
        (.createOntResource m (new AnonId (name o)))
        (.createOntResource m (ssoup-rdf/to-rdf-expanded-iri ctx o)))
      o)))

(defn clj-to-jena-property
  ([m ctx p]
   (clj-to-jena-property m ctx p true))
  ([m ctx p throw-exception-if-ns-unknown]
   (when p
     (if-let [expanded-uri (ssoup-rdf/to-rdf-expanded-iri ctx p)]
       (.createProperty m expanded-uri)
       (when throw-exception-if-ns-unknown
         (throw (Exception. (str "Cannot expand keyword " p " in the given rdf context"))))))))

(defn jena-to-clj [ctx o]
  (when o
    (if (instance? Resource o)
      (if (.isAnon ^Resource o)
        (keyword "_" (.getLabelString (.getId ^Resource o)))
        (ssoup-rdf/to-clj-iri ctx (.getURI ^Resource o)))
      (.getValue o))))

(defmethod ssoup-rdf/add-triple :ssoup-ts/jena [ts ctx triple]
  (let [m (:model ts)
        [s p o] triple
        ^Resource r (clj-to-jena m ctx s)
        ^Property pr (clj-to-jena-property m ctx p)
        jo (clj-to-jena m ctx o)]
    (.addProperty r pr jo)))

(defmethod ssoup-rdf/get-triple :ssoup-ts/jena
  [ts ctx s p]
  (let [m (:model ts)
        ^Resource r (clj-to-jena m ctx s false)
        ^Property pr (clj-to-jena-property m ctx p false)]
    (when (and r pr)
      (let [prp (when pr (.getProperty r pr))
            ^RDFNode o (when prp (.getObject prp))]
        [s p (jena-to-clj ctx o)]))))

(defmethod ssoup-rdf/list-subjects-with-property :ssoup-ts/jena
  [ts ctx p o]
  (let [m (:model ts)
        ^Property pr (clj-to-jena-property m ctx p false)
        ^RDFNode or (clj-to-jena m ctx o false)]
    (when (and pr or)
      (let [^ResIterator subjs (.listSubjectsWithProperty m pr or)
            k (iterator-seq subjs)]
        (->>
          (map
            (fn [s]
              (jena-to-clj ctx s))
            (iterator-seq subjs))
          (into []))))))

(defmethod ssoup-rdf/get-triples :ssoup-ts/jena
  ([ts ctx s]
    (ssoup-rdf/get-triples ts ctx s nil))
  ([ts ctx s p]
    (let [m (:model ts)
          ^Resource r (clj-to-jena m ctx s false)]
      (when r
        (let [stmts (if p
                      (let [^Property pr (clj-to-jena-property m ctx p false)]
                        (when pr
                          (.listProperties r pr)))
                      (.listProperties r))]
          (when stmts
            (->>
              (map (fn [^Statement stmt]
                     (let [^Property pr (.getPredicate stmt)
                           p (ssoup-rdf/to-clj-iri ctx (.getURI pr))
                           ^RDFNode o (.getObject stmt)]
                       [s p (jena-to-clj ctx o)]))
                   (iterator-seq stmts))
              (into []))))))))

(defmethod ssoup-rdf/blank-node :ssoup-ts/jena [ts]
  (let [m (:model ts)
        bnode-id (str (.getId (.createResource m)))]
    (keyword "_" bnode-id)))

(defmethod ssoup-rdf/exec-query :ssoup-ts/jena
  ([ts rdf-ctx query-string]
    (ssoup-rdf/exec-query ts rdf-ctx query-string {}))
  ([ts rdf-ctx query-string params]
    (let [^Model model (:model ts)
          pss (new ParameterizedSparqlString)]
      ; (.setBaseUri pss "http://example.org/base#")
      (doall (for [[ns uri] rdf-ctx]
               (.setNsPrefix pss (name ns) uri)))
      (.setCommandText pss query-string)
      (doseq [[^String pname value] params]
        (if (ssoup-rdf/literal? value)
          (.setLiteral pss pname ^String value)
          (when (ssoup-rdf/iri? value)
            (.setIri pss pname ^String value))))
      (let [query (.asQuery pss)]
        (with-open [qexec (QueryExecutionFactory/create query model)]
          (let [^ResultSet results (.execSelect qexec)]
            ;(ResultSetFormatter/out results query)
            (doall
              (iterator-seq
                (reify java.util.Iterator
                  (hasNext [this] (.hasNext results))
                  (next [this] (let [r (.next results)
                                     vars (.getResultVars results)]
                                 (into {} (map (fn [var] [(keyword var) (jena-to-clj rdf-ctx (.get r var))]) vars))))
                  (remove [this] (.remove results)))))))))))

(defmethod ssoup-rdf/exec-query-single-result :ssoup-ts/jena
  ([ts rdf-ctx query-string]
    (ssoup-rdf/exec-query-single-result ts rdf-ctx query-string {}))
  ([ts rdf-ctx query-string params]
    (let [results (ssoup-rdf/exec-query ts rdf-ctx query-string params)]
      (cond
        (empty? results) nil
        (= 1 (count results)) (first results)
        :else (throw (Exception. "The query returns more than one result."))))))

(defmethod ssoup-rdf/get-data-by-type :ssoup-ts/jena [ts options]
  (ssoup-rdf/exec-query ts
                        "SELECT ?y
                         WHERE
                         {?y vcard:Given ?g .}"
                        {"g" "John Smith"}))

; -------------------------------------------------------
; IOntology
; -------------------------------------------------------

(defmethod ssoup-rdf/list-types :ssoup-ts/jena
  [ts ctx]
  (let [^OntModel m (:model ts)
        classes (.listNamedClasses m)]
    (map (fn [^OntClass class]
           (jena-to-clj ctx class))
         (iterator-seq classes))))

(defmethod ssoup-rdf/get-declared-properties :ssoup-ts/jena
  ([ts ctx cl]
    (ssoup-rdf/get-declared-properties ts ctx cl false))
  ([ts ctx cl direct]
    (let ^OntModel [m (:model ts)
                    ciri (ssoup-rdf/to-rdf-expanded-iri ctx cl)
                    ^OntResource ors (.getOntResource m ciri) ; (clj-to-jena-ont m ctx cl)
                    ^OntClass ocl (.asClass ors)
                    props (.listDeclaredProperties ocl (boolean direct))]
      (map (fn [[k v]] (.println System/out k)) (iterator-seq props))
      (->>
        (iterator-seq props)
        (filter (fn [^OntProperty prop]
                  ;(.println System/out (str "prop: " prop ":" (.getDomain prop)))
                  (let [domain (jena-to-clj ctx (.getDomain prop))]
                    (and
                      ; strong restrinction !! in many
                      ; ontologies the domain is not set, but in ssoup it is required or
                      ; the property would be applied to all types !!
                      (not (nil? domain))
                      (not= :rdfs/Resource domain)
                      (not= :owl/Thing domain)

                      (not (nil? (.getRange prop)))
                      (not (.hasLiteral prop
                                        ^Property (.getProperty m "http://www.w3.org/2002/07/owl#" "deprecated")
                                        true))))))
        (map (fn [^OntProperty prop]
               {:id         (jena-to-clj ctx prop)
                :rdf/type   :rdf/Property
                :rdfs/range (jena-to-clj ctx (.getRange prop))
                :rdfs/label (or (.getLabel prop "en") (.getLabel prop nil))}))
        (into [])))))

(defmethod ssoup-rdf/get-super-types :ssoup-ts/jena
  [ts ctx cl]
  (let [^OntModel m (:model ts)
        ;^Dataset dataset (:dataset ts)
        ;_ (.write m System/out "NQuads")
        ;_ (println (.isEmpty m) "  " (.size m))
        ;_ (doall (map #(println %) (iterator-seq (.listNames dataset))))
        ;m2 (.getNamedModel dataset "http://ssoup.eu/User")
        ;m2 (ModelFactory/createOntologyModel
        ;      OntModelSpec/OWL_DL_MEM_RDFS_INF m2)
        ;_ (println " is empty " (.isEmpty m2) "  " (.size m2))
        ; _ (.write m2 System/out "N-Quads")
        ;_ (RDFDataMgr/write System/out m2 RDFFormat/NTRIPLES)
        ;_ (.flush System/out)
        ^String ciri (ssoup-rdf/to-rdf-expanded-iri ctx cl)
        ^OntResource ors (.getOntResource m ciri)           ; (clj-to-jena-ont m2 ctx cl)
        ^OntClass ocl (.asClass ors)
        scls (.listSuperClasses ocl true)]
    (->>
      (iterator-seq scls)
      (map (fn [^OntClass scl]
             (jena-to-clj ctx scl)))
      (into []))))

(defmethod ssoup-rdf/get-schema :ssoup-ts/jena
  [ts ctx]
  (let [types (ssoup-rdf/list-types ts ctx)]
    (let [y (into {}
                  (map (fn [type]
                         [type {:rdfs/label        (ssoup-rdf/get-triple-object ts ctx type :rdfs/label)
                                :rdfs/comment      (ssoup-rdf/get-triple-object ts ctx type :rdfs/comment)
                                :ssoup/properties  (ssoup-rdf/get-declared-properties ts ctx type false)
                                :ssoup/super-types (ssoup-rdf/get-super-types ts ctx type)}])
                       types))]
      y)))
