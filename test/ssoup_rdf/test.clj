(ns ssoup-rdf.test
  (:use clojure.test)
  (:require [ssoup-rdf.jena-ts]
            [ssoup-rdf.core :as ssoup-rdf])
  (:import (java.util Date)
           (org.apache.jena.rdf.model ModelFactory Model)
           (org.apache.jena.query QueryFactory QueryExecutionFactory ResultSetFormatter ParameterizedSparqlString)
           (org.apache.jena.ontology OntModelSpec)))

(def ^:dynamic *ctx*)
(def ^:dynamic *ts*)

(use-fixtures
  :each (fn [f]
          (binding [*ctx* (merge ssoup-rdf/schema-org-context {:test "http://test/#"
                                                               :noise "http://test/noise#"})
                    *ts* (ssoup-rdf.jena-ts/new-plain-jena-triple-store "test-ts" #{"tag-1" "tag-2"})]
            (try (f)
                 (finally
                   (ssoup-rdf/close *ts*))))))

(deftest test-expand-iri
  (is (= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
         (ssoup-rdf/to-rdf-expanded-iri *ctx* :rdf/type))))

(deftest test-compact-iri
  (is (= :rdf/type
         (ssoup-rdf/to-clj-iri *ctx* "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"))))

(deftest test-add-get-triple
  (is (let [s :test/obj
            p :rdf/type
            o :test/TestType
            t [s p o]]
        (ssoup-rdf/add-triple *ts* *ctx* t)
        (let [actual (ssoup-rdf/get-triple *ts* *ctx* s p)]
          (= t
             actual)))))

(def obj {:id       :test/obj
          :rdf/type :test/TestType
          :test/a1  "sv1"
          :test/a2  "sv2"
          :test/a3  {:rdf/type :test/TestType1
                     :test/a4  "sv3"
                     :test/a5  "sv4"}})

(deftest test-push
  (is (let [s :test/obj
            p :rdf/type
            o :test/TestType
            t [s p o]]
        (ssoup-rdf/push *ts* *ctx* {:id s
                                    p   o})
        (let [actual (ssoup-rdf/get-triple *ts* *ctx* s p)]
          (= t
             actual))))
  (is (let [s :test/obj]
        (ssoup-rdf/push *ts* *ctx* obj)
        ; (ssoup-rdf/print-all *ts* "TTL")
        (let [actual (ssoup-rdf/get-triple *ts* *ctx* s :test/a2)]
          (= [s :test/a2 "sv2"]
             actual)))))

(deftest test-pull
  (let [s :test/obj
        p :rdf/type
        o :test/TestType
        t [s p o]]
    (ssoup-rdf/add-triple *ts* *ctx* t)
    (is
      (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type])]
        (= {:id s
            p   o}
           actual)))
    ; pull non existing predicate
    (is
      (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/na])]
        (= s
           actual)))))

(deftest test-push-pull
  (let [s (:id obj)]
    (ssoup-rdf/push *ts* *ctx* obj)
    (is
      (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 :test/a2 {:test/a3 [:rdf/type :test/a4 :test/a5]}])]
        (= actual obj)))
    (is (let [s (:id obj)]
          ;(ssoup-rdf/print-all *ts* "TTL")
          (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 :test/a2 {:test/a3 ["*" 10]}])]
            (= actual obj))))))

(def obj2 {:id       :test/obj2
           :rdf/type :test/TestType
           :test/a1  "sv1"
           :test/a2  #{"sv2"
                       "sv3"}})

(def obj3 {:id       :test/obj3
           :rdf/type :test/TestType
           :test/a1  "sv1"
           :test/a2  #{"sv2"
                       "sv3"
                       "sv4"}
           :test/a3  #{{:rdf/type :test/TestType1
                        :test/a4  "sv5"
                        :test/a5  "sv6"
                        :test/a6  5
                        :test/a7  "sv7"}
                       {:rdf/type :test/TestType2
                        :test/a4  "sv8"
                        :test/a5  "sv9"
                        :test/a7  "sv10"}}})

(deftest test-push-pull-mp
  (is (let [s (:id obj2)]
        (ssoup-rdf/push *ts* *ctx* obj2)
        ;(ssoup-rdf/print-all *ts* "TTL")
        (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 :test/a2])]
          (= actual obj2))))
  (let [s (:id obj3)]
    (ssoup-rdf/push *ts* *ctx* obj3)
    (is
      ;(ssoup-rdf/print-all *ts* "TTL")
      (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 :test/a2 {:test/a3 [:rdf/type :test/a4 :test/a5 :test/a6 :test/a7]}])]
        (= actual obj3)))
    (is
      ;(ssoup-rdf/print-all *ts* "TTL")
      (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 :test/a2 {:test/a3 ["*"]}])]
        (= actual obj3)))))

(def obj4 {:id       :test/obj4
           :rdf/type :test/TestType
           :test/a1  "sv1"
           :test/a2  #{:test/kv1}})

(deftest test-options
  (let [s (:id obj4)]
    (ssoup-rdf/push *ts* *ctx* obj4)
    (is (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 :test/a2])]
          (= actual (assoc obj4 :test/a2 :test/kv1))))
    (is (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 :test/a2] {:attrs {:test/a2 [:ssoup/as-set]}})]
          (= actual obj4)))))

(def obj-with-array-1 {:id       :test/obj-with-array-1
                       :rdf/type :test/TestType
                       :test/a1  "sv1"
                       :test/a2  [:test/kv1 :test/kv2 :test/kv3]})

(deftest test-array-1
  (let [s (:id obj-with-array-1)]
    (ssoup-rdf/push *ts* *ctx* obj-with-array-1)
    (is (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 {:test/a2 ["*"]}])]
          (= actual obj-with-array-1)))))

(def obj-with-array-2 {:id       :test/obj-with-array-2
                       :rdf/type :test/TestType
                       :test/a1  "sv1"
                       :test/a2  [{:id      :test/ao1
                                   :test/a4 :test/kv1}
                                  "sv2"
                                  {:id      :test/ao2
                                   :test/a4 :test/kv3}]})

(deftest test-array-2
  (let [s (:id obj-with-array-2)]
    (ssoup-rdf/push *ts* *ctx* obj-with-array-2)
    (is (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 {:test/a2 ["*" 2]}])]
          (= actual obj-with-array-2)))))

(def obj-with-array-3 {:id       :test/obj-with-array-3
                       :rdf/type :test/TestType
                       :test/a1  "sv1"
                       :test/a2  [{:id      :test/ao1
                                   :test/a4 :test/kv1}
                                  "sv2"
                                  {:id      :test/ao2
                                   :test/a4 {:id      :test/ao4
                                             :test/a5 :test/kv3}}]})

(deftest test-array-3
  (let [s (:id obj-with-array-3)]
    (ssoup-rdf/push *ts* *ctx* obj-with-array-3)
    (is (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 {:test/a2 ["*" 2]}])]
          (= actual {:id       :test/obj-with-array-3
                     :rdf/type :test/TestType
                     :test/a1  "sv1"
                     :test/a2  [{:id      :test/ao1
                                 :test/a4 :test/kv1}
                                "sv2"
                                {:id      :test/ao2
                                 :test/a4 :test/ao4}]})))
    (is (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type :test/a1 {:test/a2 ["*" 3]}])]
          (= actual obj-with-array-3)))))

(def obj-with-array-4 {:id       :test/obj-with-array-4
                       :rdf/type :test/TestType
                       :test/a1  [{:id      :test/ao0
                                   :test/a4 {:id      :test/a8
                                             :test/a5 "sv4"}}]
                       :test/a2  "sv2"
                       :test/a3  [{:id      :test/ao1
                                   :test/a4 :test/kv1}
                                  "sv3"
                                  {:id      :test/ao2
                                   :test/a4 {:id      :test/ao4
                                             :test/a5 :test/kv3}}]})

(deftest test-array-4
  (let [s (:id obj-with-array-4)]
    (ssoup-rdf/push *ts* *ctx* obj-with-array-4)
    (is (let [actual (ssoup-rdf/pull *ts* *ctx* s ["*" 4])]
          (= actual obj-with-array-4)))
    (is (let [actual (ssoup-rdf/pull *ts* *ctx* s [:rdf/type {:test/a1 ["*" 3]} :test/a2 {:test/a3 ["*" 3]}])]
          (= actual obj-with-array-4)))))

(deftest test-build-query
  (let [q {:query  {:select   :?s
                    :where    [[:?s :rdf/type :test/TestType]
                               [:?s :test/value :?v]]
                    :order-by [[:?v :asc]]}
           :offset 0
           :limit  10}]
    (is (let [actual (ssoup-rdf/build-query-string q)]
          (= actual "select ?s where { ?s rdf:type test:TestType . ?s test:value ?v . } order by asc(?v) limit 10 offset 0")))
    (is (let [actual (ssoup-rdf/build-count-query-string q)]
          (= actual "select (count(*) as ?count) where { ?s rdf:type test:TestType . ?s test:value ?v . }")))))

(deftest test-pull-many
  (doall (for [i (range 1000)]
           (ssoup-rdf/push *ts* *ctx* {:id         (keyword "test" (str "obj-" i))
                                       :rdf/type   :test/TestType
                                       :test/value i})))
  (doall (for [i (range 1000)]
           (ssoup-rdf/push *ts* *ctx* {:id         (keyword "noise" (str "obj-" i))
                                       :rdf/type   :test/Noise
                                       :test/value i})))
  (is (let [actual (ssoup-rdf/pull-many *ts* *ctx*
                                        {:query  {:select   :?s
                                                  :where    [[:?s :rdf/type :test/TestType]
                                                             [:?s :test/value :?v]]
                                                  :order-by [[:?v :asc]]}
                                         :offset 0
                                         :limit  10}
                                        [:rdf/type :test/value])]
        (= actual {:ssoup/from       0
                   :ssoup/to         9
                   :ssoup/totalCount 1000
                   :ssoup/results    (for [i (range 0 10)]
                                 {:id         (keyword "test" (str "obj-" i))
                                  :rdf/type   :test/TestType
                                  :test/value i})})))
  (is (let [actual (ssoup-rdf/pull-many *ts* *ctx*
                                        {:query  {:select   :?s
                                                  :where    [[:?s :rdf/type :test/TestType]
                                                             [:?s :test/value :?v]]
                                                  :order-by [[:?v :asc]]}
                                         :offset 10
                                         :limit  10}
                                        [:rdf/type :test/value])]
        (= actual {:ssoup/from       10
                   :ssoup/to         19
                   :ssoup/totalCount 1000
                   :ssoup/results    (for [i (range 10 20)]
                                       {:id         (keyword "test" (str "obj-" i))
                                        :rdf/type   :test/TestType
                                        :test/value i})})))
  (is (let [actual (ssoup-rdf/pull-many *ts* *ctx*
                                        {:query  {:select   :?s
                                                  :where    [[:?s :rdf/type :test/TestType]
                                                             [:?s :test/value :?v]]
                                                  :order-by [[:?v :asc]]}
                                         :offset 990
                                         :limit  10}
                                        [:rdf/type :test/value])]
        (= actual {:ssoup/from       990
                   :ssoup/to         999
                   :ssoup/totalCount 1000
                   :ssoup/results    (for [i (range 990 1000)]
                                 {:id         (keyword "test" (str "obj-" i))
                                  :rdf/type   :test/TestType
                                  :test/value i})})))
  (is (let [actual (ssoup-rdf/pull-many *ts* *ctx*
                                        {:query  {:select   :?s
                                                  :where    [[:?s :rdf/type :test/TestType]
                                                             [:?s :test/value :?v]]
                                                  :order-by [[:?v :desc]]}
                                         :offset 0
                                         :limit  10}
                                        [:rdf/type :test/value])]
        (= actual {:ssoup/from       0
                   :ssoup/to         9
                   :ssoup/totalCount 1000
                   :ssoup/results    (for [i (reverse (range 990 1000))]
                                       {:id         (keyword "test" (str "obj-" i))
                                        :rdf/type   :test/TestType
                                        :test/value i})}))))
